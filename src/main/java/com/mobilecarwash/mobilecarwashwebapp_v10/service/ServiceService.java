package com.mobilecarwash.mobilecarwashwebapp_v10.service;

import com.mobilecarwash.mobilecarwashwebapp_v10.data.models.Service;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.request.ServiceRequest;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.response.MessageResponse;
import com.mobilecarwash.mobilecarwashwebapp_v10.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface ServiceService {

    MessageResponse createService(ServiceRequest serviceRequest);
    Optional<Service> updateService(Integer serviceId, ServiceRequest serviceRequest) throws ResourceNotFoundException;
    void deleteService(Integer serviceId) throws ResourceNotFoundException;
    Service getASingleService(Integer serviceId) throws ResourceNotFoundException;
    List<Service> getAllService();

}
