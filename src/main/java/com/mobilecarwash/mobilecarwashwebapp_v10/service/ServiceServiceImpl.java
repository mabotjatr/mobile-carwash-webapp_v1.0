package com.mobilecarwash.mobilecarwashwebapp_v10.service;

import com.mobilecarwash.mobilecarwashwebapp_v10.data.models.Service;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.request.ServiceRequest;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.response.MessageResponse;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.mobilecarwash.mobilecarwashwebapp_v10.exceptions.ResourceNotFoundException;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    @Autowired
    ServiceRepository serviceRepository;

    @Override
    public MessageResponse createService(ServiceRequest serviceRequest) {

        Service newService = new Service();
        newService.setName(serviceRequest.getName());
        newService.setVehicle_type(serviceRequest.getVehicle_type());
        newService.setPrice(serviceRequest.getPrice());
        serviceRepository.save(newService);
        return new MessageResponse("New Service created successfully");

    }

    @Override
    public Optional<Service> updateService(Integer serviceId, ServiceRequest serviceRequest) throws ResourceNotFoundException{

        Optional<Service> service = serviceRepository.findById(serviceId);

        if(service.isPresent()){
            service.get().setPrice(serviceRequest.getPrice());
            service.get().setVehicle_type(serviceRequest.getVehicle_type());
            service.get().setName(serviceRequest.getName());
            serviceRepository.save(service.get());
        }else {
            throw new ResourceNotFoundException("Error getting service by Id; ID:"+ serviceId);
        }

        return service;
    }

    @Override
    public void deleteService(Integer serviceId)  throws ResourceNotFoundException{
        if(serviceRepository.getById(serviceId).getId().equals(serviceId)){
            serviceRepository.deleteById(serviceId);
        }else{
            throw new ResourceNotFoundException("Error deleting service; Id:"+serviceId);
        }
    }

    @Override
    public Service getASingleService(Integer serviceId) throws ResourceNotFoundException {
        return serviceRepository.findById(serviceId).orElseThrow(() -> new ResourceNotFoundException("Error finding a Service; Id:"+serviceId));
    }

    @Override
    public List<Service> getAllService() {
        return serviceRepository.findAll();
    }
}
