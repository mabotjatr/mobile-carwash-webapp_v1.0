package com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ServiceRequest {

    @NotBlank
    @NotNull
    private String name;
    @NotBlank
    @NotNull
    private String vehicle_type;
    @NotBlank
    @NotNull
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


}
