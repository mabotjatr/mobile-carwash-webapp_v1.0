package com.mobilecarwash.mobilecarwashwebapp_v10.data.repository;

import com.mobilecarwash.mobilecarwashwebapp_v10.data.models.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer> {


}
