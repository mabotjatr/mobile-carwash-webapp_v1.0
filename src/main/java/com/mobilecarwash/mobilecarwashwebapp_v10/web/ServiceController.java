package com.mobilecarwash.mobilecarwashwebapp_v10.web;

import com.mobilecarwash.mobilecarwashwebapp_v10.data.models.Service;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.request.ServiceRequest;
import com.mobilecarwash.mobilecarwashwebapp_v10.data.payloads.response.MessageResponse;
import com.mobilecarwash.mobilecarwashwebapp_v10.exceptions.ResourceNotFoundException;
import com.mobilecarwash.mobilecarwashwebapp_v10.service.ServiceService;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/service")
@ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 400, message = "This is a bad request, please follow the API documentation for the proper request format"),
        @io.swagger.annotations.ApiResponse(code = 401, message = "Due to security constraints, your access request cannot be authorized"),
        @io.swagger.annotations.ApiResponse(code = 500, message = "The server is down. Please bear with us."),
})
public class ServiceController {

    @Autowired
    ServiceService serviceService;

    @GetMapping("/all")
    public ResponseEntity<List<Service>> getAllServices() {
        List<Service> services = serviceService.getAllService();
        return new ResponseEntity<>(services, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Service> getServiceById(@PathVariable("id") Integer id){
        Service service = new Service();
        try{
            service = serviceService.getASingleService(id);
        }catch (ResourceNotFoundException e){
            System.out.println(e.getMessage());
        }

        return  new ResponseEntity<>(service, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addService(@RequestBody ServiceRequest service) {
        MessageResponse newService = serviceService.createService(service);
        return new ResponseEntity<>(newService, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public Optional<Service> updateEmployee(@PathVariable Integer id, @RequestBody ServiceRequest serviceRequest) {
        return serviceService.updateService(id, serviceRequest);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteService(@PathVariable("id") Integer id) {
        serviceService.deleteService(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
